package com.github.ramonrabello.lib.model.util;

import android.location.Location;

/**
 * Created by ramonrabello on 25/11/16.
 */

public final class LocationUtils {

    /**
     * Min distance between photo location and
     * user device location.
     */
    private static final int MIN_DISTANCE = 20000;

    public static float distanceInMeters(Location l1, Location l2){
        return l1.distanceTo(l2);
    }

    public static float distanceInKm(Location l1, Location l2){
        return distanceInMeters(l1,l2) / 1000;
    }

    public static boolean isNear(Location l1, Location l2){
        return distanceInKm(l1, l2) <= MIN_DISTANCE;
    }
}