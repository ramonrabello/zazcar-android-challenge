package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 25/11/16.
 */
public class Usage {

    @SerializedName("candownload")
    private boolean canDownload;

    @SerializedName("canblog")
    private boolean canBlog;

    @SerializedName("canprint")
    private boolean canPrint;

    @SerializedName("canshare")
    private boolean canShare;

    public boolean isCanDownload() {
        return canDownload;
    }

    public void setCanDownload(boolean canDownload) {
        this.canDownload = canDownload;
    }

    public boolean isCanBlog() {
        return canBlog;
    }

    public void setCanBlog(boolean canBlog) {
        this.canBlog = canBlog;
    }

    public boolean isCanPrint() {
        return canPrint;
    }

    public void setCanPrint(boolean canPrint) {
        this.canPrint = canPrint;
    }

    public boolean isCanShare() {
        return canShare;
    }

    public void setCanShare(boolean canShare) {
        this.canShare = canShare;
    }
}
