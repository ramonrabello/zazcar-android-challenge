package com.github.ramonrabello.lib.view.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Base adapter that also handles the item click and adds some
 * interesting adapter operations like add, move, change and remove and
 * listeners for each operation.
 */
public abstract class BaseAdapter<VH extends BaseViewHolder, T> extends RecyclerView.Adapter<VH> implements BaseViewHolder.OnItemClickCallback {

    private List<T> collection = new ArrayList<>();
    private OnAddItemListener onAddItemListener;
    private OnRemoveItemListener onRemoveItemListener;
    private OnChangeItemListener onChangeItemListener;
    private OnMoveItemListener onMoveItemListener;

    /**
     * Constructs the adapter using
     * a known collection.
     *
     * @param collection The collection of the adapter.
     */
    public BaseAdapter(List<T> collection) {
        this.collection = collection;
    }

    /**
     * Default constructor without any collection.
     * Please make sure to call {@link BaseAdapter#addItem(Object)}
     * in order to fill the adater with collection.
     */
    public BaseAdapter() {
    }

    /**
     * Sets the listener for adding items
     * in the adapter.
     *
     * @param onAddItemListener
     */
    public void setOnAddItemListener(OnAddItemListener onAddItemListener) {
        this.onAddItemListener = onAddItemListener;
    }

    /**
     * Sets the listener for removing items
     * in the adapter.
     *
     * @param onRemoveItemListener
     */
    public void setOnRemoveItemListener(OnRemoveItemListener onRemoveItemListener) {
        this.onRemoveItemListener = onRemoveItemListener;
    }

    /**
     * Sets the listener for items change
     * in the adapter.
     *
     * @param onChangeItemListener
     */
    public void setOnChangeItemListener(OnChangeItemListener onChangeItemListener) {
        this.onChangeItemListener = onChangeItemListener;
    }

    /**
     * Sets the listener that notifies when items
     * move int the adapter.
     *
     * @param onMoveItemListener
     */
    public void setOnMoveItemListener(OnMoveItemListener onMoveItemListener) {
        this.onMoveItemListener = onMoveItemListener;
    }

    /**
     * Retrieves the adapter data collection.
     *
     * @return The collection of items.
     */
    protected List<T> getCollection() {
        return collection;
    }

    /**
     * Get the item from the <code>itemPosition</code>.
     *
     * @return The item at position.
     */
    protected T getItem(int itemPosition) {
        return !collection.isEmpty() ? collection.get(itemPosition) : null;
    }

    /***
     * Returns the number of items in the adapter.
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return collection.size();
    }

    /**
     * Adds an item to the adapter.
     *
     * @param item The item to de added.
     */
    public void addItem(T item) {
        collection.add(item);
        notifyItemInserted(collection.size() - 1);
        onAddItemListener.onItemAdded(collection.size() - 1);
    }

    /**
     * Adds a new collection of items.
     *
     * @param items The items to de added.
     */
    public void addItems(List<T> items) {
        collection.addAll(items);
        notifyDataSetChanged();

        if (onAddItemListener != null){
            onAddItemListener.onItemAdded(collection.size() - 1);
        }
    }

    /**
     * Moves an item from <code>fromPosition</code>
     * to <code>toPosition</code>.
     *
     * @param fromPosition The position from where the item will be moved.
     * @param toPosition   The position to where the item will be moved.
     */
    public void moveItem(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);

        if (onMoveItemListener != null){
            onMoveItemListener.onItemMoved(fromPosition, toPosition);
        }
    }

    /**
     * Updates a item in the <code>itemPosition</code>.
     *
     * @param itemPosition The position from where the item will be moved.
     */
    public void updateItem(int itemPosition) {
        notifyItemChanged(itemPosition);

        if (onChangeItemListener != null) {
            onChangeItemListener.onItemChanged(itemPosition);
        }
    }

    /**
     * Removes the item in the <code>itemPosition</code>.
     *
     * @param itemPosition The position from where the item will be moved.
     * @return The object removed from the adapter.
     */
    public T removeItem(int itemPosition) {
        T removedItem = collection.remove(itemPosition);
        if (removedItem != null) {
            notifyItemInserted(itemPosition);

            if (onRemoveItemListener != null){
                onRemoveItemListener.onItemRemoved(itemPosition);
            }
        }
        return removedItem;
    }

    /**
     * Removes the item from the adapter.
     *
     * @param item The item to be removed.
     * @return The object removed from the collection.
     */
    public boolean removeItem(T item) {
        boolean itemRemoved = collection.remove(item);
        if (itemRemoved) {
            notifyDataSetChanged();
        }
        return itemRemoved;
    }

    /**
     * Clears the collection.
     */
    public void clear() {
        collection.clear();
        notifyDataSetChanged();
    }

    /**
     * Listens when the item is added.
     */
    interface OnAddItemListener {

        /**
         * Called when an item is added to the
         * adapter.
         *
         * @param newPosition The new item position.
         */
        void onItemAdded(int newPosition);
    }

    /**
     * Listens when the item is removed.
     */
    interface OnRemoveItemListener {

        /**
         * Called when an item is removed from the
         * adapter.
         *
         * @param itemPosition The item position of removed item.
         */
        void onItemRemoved(int itemPosition);
    }

    /**
     * Listens when the item has been changed.
     */
    interface OnChangeItemListener {

        /**
         * Called when an item has been changed
         * inside the adapter.
         *
         * @param itemPosition The item position of changed item.
         */
        void onItemChanged(int itemPosition);
    }

    /**
     * Listens when the item has been moved.
     */
    interface OnMoveItemListener {

        /**
         * Called when an item has been moved
         * inside the adapter.
         *
         * @param fromPosition The initial item position.
         * @param toPosition   The final item position.
         */
        void onItemMoved(int fromPosition, int toPosition);
    }
}