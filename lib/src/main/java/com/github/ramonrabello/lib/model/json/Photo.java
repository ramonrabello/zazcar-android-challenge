package com.github.ramonrabello.lib.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Locale;

/**
 * Created by ramonrabello on 23/11/16.
 */

public class Photo implements Parcelable {

    private static final String BASE_URL = "https://farm%d.staticflickr.com/%s/%s_%s";
    private static final String PAGE_URL = "https://www.flickr.com/photos/%s/%s/";
    private static final String JPG_EXTENSION = ".jpg";
    private static final String ORIGINAL_URL = BASE_URL.concat(JPG_EXTENSION);
    private static final String SMALL_SIZE_URL = BASE_URL.concat("_s").concat(JPG_EXTENSION);
    private static final String MEDIUM_SIZE_URL = BASE_URL.concat("_c").concat(JPG_EXTENSION);
    private static final String LARGE_SIZE_URL = BASE_URL.concat("_q").concat(JPG_EXTENSION);

    private String id;
    private String owner;
    private String secret;
    private String server;
    private int farm;
    private String title;


    @SerializedName("ispublic")
    private boolean isPublic;

    @SerializedName("isfriend")
    private boolean isFriend;

    @SerializedName("isfamily")
    private boolean isFamily;

    @Expose(deserialize = false)
    private String authorName;

    @Expose(deserialize = false)
    private String authorDescription;

    @Expose(deserialize = false)
    private String authorBuddyIconUrl;
    @Expose(deserialize = false)
    private int viewsCount;
    @Expose(deserialize = false)
    private int favesCount;
    @Expose(deserialize = false)
    private int commentsCount;

    public int getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public int getFavesCount() {
        return favesCount;
    }

    public void setFavesCount(int favesCount) {
        this.favesCount = favesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorDescription() {
        return authorDescription;
    }

    public void setAuthorDescription(String authorDescription) {
        this.authorDescription = authorDescription;
    }

    public String getAuthorBuddyIconUrl() {
        return authorBuddyIconUrl;
    }

    public void setAuthorBuddyIconUrl(String authorBuddyIconUrl) {
        this.authorBuddyIconUrl = authorBuddyIconUrl;
    }

    public Photo(){}

    public Photo(Parcel in) {
        id = in.readString();
        owner = in.readString();
        secret = in.readString();
        server = in.readString();
        farm = in.readInt();
        title = in.readString();
        isPublic = in.readByte() != 0;
        isFriend = in.readByte() != 0;
        isFamily = in.readByte() != 0;
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getFarm() {
        return farm;
    }

    public void setFarm(int farm) {
        this.farm = farm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isFriend() {
        return isFriend;
    }

    public void setFriend(boolean friend) {
        isFriend = friend;
    }

    public boolean isFamily() {
        return isFamily;
    }

    public void setFamily(boolean family) {
        isFamily = family;
    }

    public String getPageUrl(){
        return String.format(Locale.getDefault(), PAGE_URL, owner, id);
    }

    public String getUrl(){
        return String.format(Locale.getDefault(), ORIGINAL_URL, farm, server, id, secret);
    }

    public String getSmallSizeUrl(){
        return String.format(Locale.getDefault(), SMALL_SIZE_URL, farm, server, id, secret);
    }

    public String getMediumSizeUrl(){
        return String.format(Locale.getDefault(), MEDIUM_SIZE_URL, farm, server, id, secret);
    }

    public String getLargeSizeUrl(){
        return String.format(Locale.getDefault(), LARGE_SIZE_URL, farm, server, id, secret);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(owner);
        parcel.writeString(secret);
        parcel.writeString(server);
        parcel.writeInt(farm);
        parcel.writeString(title);
        parcel.writeByte((byte) (isPublic ? 1 : 0));
        parcel.writeByte((byte) (isFriend ? 1 : 0));
        parcel.writeByte((byte) (isFamily ? 1 : 0));
    }
}
