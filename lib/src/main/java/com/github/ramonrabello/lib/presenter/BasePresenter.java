package com.github.ramonrabello.lib.presenter;


import com.github.ramonrabello.lib.view.BaseView;

/**
 * Base interface for presenters.
 */
public interface BasePresenter<T> {

    /**
     *
     */
    void onLoadedData();

    /**
     *
     */
    void onFinish();

    /**
     *
     * @param view
     */
    void onUpdateView(BaseView<T> view);
}
