package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 25/11/16.
 */

public class Tag {

    private String id;
    private String author;
    private String authorname;
    private String raw;

    @SerializedName("_content")
    private String content;

    @SerializedName("machine_tag")
    private int machineTag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorname() {
        return authorname;
    }

    public void setAuthorname(String authorname) {
        this.authorname = authorname;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getMachineTag() {
        return machineTag;
    }

    public void setMachineTag(int machineTag) {
        this.machineTag = machineTag;
    }
}
