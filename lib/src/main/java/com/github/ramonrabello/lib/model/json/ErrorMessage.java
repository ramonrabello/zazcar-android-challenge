package com.github.ramonrabello.lib.model.json;

/**
 * Created by ramonrabello on 22/11/16.
 */

public class ErrorMessage {

    private String stat;
    private int code;
    private String message;

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
