package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 24/11/16.
 */

public class PersonPhotos {

    @SerializedName("firstdatetaken")
    private Content firstDateTaken;

    @SerializedName("firstdate")
    private Content firstDate;

    @SerializedName("count")
    private Content count;

    public Content getFirstDateTaken() {
        return firstDateTaken;
    }

    public void setFirstDateTaken(Content firstDateTaken) {
        this.firstDateTaken = firstDateTaken;
    }

    public Content getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(Content firstDate) {
        this.firstDate = firstDate;
    }

    public Content getCount() {
        return count;
    }

    public void setCount(Content count) {
        this.count = count;
    }
}
