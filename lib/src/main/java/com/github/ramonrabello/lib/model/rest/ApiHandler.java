package com.github.ramonrabello.lib.model.rest;

import com.github.ramonrabello.lib.model.json.typeadapter.IntToBooleanTypeAdapter;
import com.github.ramonrabello.lib.model.rest.interceptor.LoggingInterceptor;
import com.github.ramonrabello.lib.model.rest.interceptor.RequestInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Handler for the Flickr API.
 */

public class ApiHandler {

    /**
     * Method that retrieves the Retrofit Endpoint interface to call
     * Flickr API services.
     *
     * @return
     */
    public static FlickrApi getApi(){

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(boolean.class, new IntToBooleanTypeAdapter())
                .create();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new RequestInterceptor())
                .addInterceptor(new LoggingInterceptor())
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FlickrApi.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        return retrofit.create(FlickrApi.class);
    }
}