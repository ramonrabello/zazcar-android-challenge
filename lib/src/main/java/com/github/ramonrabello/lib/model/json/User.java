package com.github.ramonrabello.lib.model.json;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Mapping of "user" attribute in Tags Endpoint.
 */
public class User implements Parcelable {

    private String username;
    private String profilePicture;
    private String id;
    private String fullName;

    protected User(Parcel in) {
        username = in.readString();
        profilePicture = in.readString();
        id = in.readString();
        fullName = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(profilePicture);
        parcel.writeString(id);
        parcel.writeString(fullName);
    }
}
