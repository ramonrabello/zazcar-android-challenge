package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 25/11/16.
 */

public class Editability {

    @SerializedName("cancomment")
    private boolean canComment;

    @SerializedName("canaddmeta")
    private boolean canAddMeta;

    public boolean isCanComment() {
        return canComment;
    }

    public void setCanComment(boolean canComment) {
        this.canComment = canComment;
    }

    public boolean isCanAddMeta() {
        return canAddMeta;
    }

    public void setCanAddMeta(boolean canAddMeta) {
        this.canAddMeta = canAddMeta;
    }
}
