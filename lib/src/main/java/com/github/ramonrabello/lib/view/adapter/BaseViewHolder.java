package com.github.ramonrabello.lib.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Base class for ViewHolders that also
 * handles item clicks in a adapter.
 */

public abstract class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private OnItemClickCallback onItemClickCallback;

    public BaseViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @Override
    public void onClick(View view) {
        onItemClickCallback.onItemClick(view, getAdapterPosition());
    }

    /**
     *
     */
    interface OnItemClickCallback {
        void onItemClick(View view, int position);
    }

}
