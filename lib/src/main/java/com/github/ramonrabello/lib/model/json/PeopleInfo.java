package com.github.ramonrabello.lib.model.json;

/**
 * Created by ramonrabello on 24/11/16.
 */
public class PeopleInfo {

    
    private Person person;
    private String stat;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public boolean isStatOk(){
        return stat != null && stat.equals("ok");
    }

    public boolean isStatFail(){
        return stat != null && stat.equals("fail");
    }
}
