package com.github.ramonrabello.lib.model.json.typeadapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Type adapter that convert ints to booleans.
 */
public class IntToBooleanTypeAdapter implements JsonDeserializer<Boolean> {

    public Boolean deserialize(JsonElement json, Type typeOfT,
                               JsonDeserializationContext context) throws JsonParseException
    {
        int code = json.getAsInt();

        if (code == 0){
            return false;
        } else if (code == 1){
            return true;
        } else {
            return null;
        }
    }
}
