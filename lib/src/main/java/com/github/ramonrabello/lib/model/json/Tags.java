package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ramonrabello on 25/11/16.
 */

public class Tags {

    @SerializedName("tag")
    private List<Tag> tagList;

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }
}
