package com.github.ramonrabello.lib.model.json;

/**
 * Created by ramonrabello on 26/11/16.
 */
public class PhotoStats {

    private String views;
    private String faves;
    private String comments;

    public String getViews() {
        return views != null ? views: "0";
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getFaves() {
        return faves != null ? faves: "0";
    }

    public void setFaves(String faves) {
        this.faves = faves;
    }

    public String getComments() {
        return comments != null ? comments: "0";
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
