package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 23/11/16.
 */
public class Description {

    @SerializedName("_content")
    private String content;
}
