package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 25/11/16.
 */

public class Dates {

    private String posted;
    private String taken;

    @SerializedName("takengranularity")
    private String takenGranularity;

    @SerializedName("takenunknown")
    private String takenUnknown;

    @SerializedName("lastupdate")
    private String lastUpdate;

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getTaken() {
        return taken;
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }

    public String getTakenGranularity() {
        return takenGranularity;
    }

    public void setTakenGranularity(String takenGranularity) {
        this.takenGranularity = takenGranularity;
    }

    public String getTakenUnknown() {
        return takenUnknown;
    }

    public void setTakenUnknown(String takenUnknown) {
        this.takenUnknown = takenUnknown;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
