package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ramonrabello on 25/11/16.
 */

public class Notes {

    @SerializedName("note")
    private List<Note> noteList;

    public List<Note> getNoteList() {
        return noteList;
    }

    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
    }
}
