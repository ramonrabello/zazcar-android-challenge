package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 25/11/16.
 */

public class Visibility {

    @SerializedName("ispublic")
    private boolean isPublic;

    @SerializedName("isfriend")
    private boolean isFriend;

    @SerializedName("isFamily")
    private boolean isFamily;

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isFriend() {
        return isFriend;
    }

    public void setFriend(boolean friend) {
        isFriend = friend;
    }

    public boolean isFamily() {
        return isFamily;
    }

    public void setFamily(boolean family) {
        isFamily = family;
    }
}
