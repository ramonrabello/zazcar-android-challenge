package com.github.ramonrabello.lib.model.json;

/**
 * Mapping of root JSON in flickr.photos.search endpoint of Flickr API.
 */
public class PhotoSearch {

    private Photos photos;
    private String stat;

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
