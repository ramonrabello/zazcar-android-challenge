package com.github.ramonrabello.lib.view;

import com.github.ramonrabello.lib.presenter.BasePresenter;

/**
 * Base interface for views,
 * in Model-View-Presenter architecture.
 */

public interface BaseView<T> {

    /**
     *
     * @param type
     */
    void onDataAvailable(T type);

    /**
     *
     * @param throwable
     */
    void onDataError(Throwable throwable);
}
