package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 23/11/16.
 */

public class PhotoInfo {

    @SerializedName("photo")
    private PhotoInfoItem photoInfoItem;
    private String stat;

    public PhotoInfoItem getPhotoInfoItem() {
        return photoInfoItem;
    }

    public void setPhotoInfoItem(PhotoInfoItem photoInfoItem) {
        this.photoInfoItem = photoInfoItem;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public boolean isOk(){
        return stat != null && stat.equals("ok");
    }

    public boolean isFail(){
        return stat != null && stat.equals("fail");
    }
}
