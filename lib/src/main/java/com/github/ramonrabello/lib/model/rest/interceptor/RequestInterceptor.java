package com.github.ramonrabello.lib.model.rest.interceptor;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Request interceptor to add default parameters in URL query.
 */
public class RequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        HttpUrl url = request.url()
                .newBuilder()
                .addQueryParameter("api_key", "463b5e33e093f2af27eb0274627809de")
                .addQueryParameter("nojsoncallback", "1")
                .addQueryParameter("has_geo", "1")
                .addQueryParameter("per_page", "5")
                .addQueryParameter("format", "json")
                .build();

        request = request
                .newBuilder()
                .url(url)
                .build();

        return chain.proceed(request);

    }
}
