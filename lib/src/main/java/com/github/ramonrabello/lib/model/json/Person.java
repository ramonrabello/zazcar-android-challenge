package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

import java.util.Locale;

/**
 * Created by ramonrabello on 24/11/16.
 */
public class Person {

    private String id;
    private String nsid;

    @SerializedName("ispro")
    private boolean isPro;

    @SerializedName("can_buy_pro")
    private boolean canBuyPro;

    @SerializedName("iconserver")
    private String iconServer;

    @SerializedName("iconfarm")
    private int iconFarm;

    @SerializedName("path_alias")
    private String pathAlias;

    @SerializedName("has_stats")
    private boolean hasStats;

    private String gender;

    private boolean ignored;

    private int contact;

    private int friend;

    private int family;

    private int revcontact;

    private int revfriend;

    private int revfamily;

    private Content username;

    private Content realname;

    private Content location;

    private Content description;

    @SerializedName("photosurl")
    private Content photosUrl;

    @SerializedName("profileurl")
    private Content profileUrl;

    @SerializedName("mobileurl")
    private Content mobileUrl;

    @SerializedName("photos")
    private PersonPhotos personPhotos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNsid() {
        return nsid;
    }

    public void setNsid(String nsid) {
        this.nsid = nsid;
    }

    public boolean isPro() {
        return isPro;
    }

    public void setPro(boolean pro) {
        isPro = pro;
    }

    public boolean isCanBuyPro() {
        return canBuyPro;
    }

    public void setCanBuyPro(boolean canBuyPro) {
        this.canBuyPro = canBuyPro;
    }

    public String getIconServer() {
        return iconServer;
    }

    public void setIconServer(String iconServer) {
        this.iconServer = iconServer;
    }

    public int getIconFarm() {
        return iconFarm;
    }

    public void setIconFarm(int iconFarm) {
        this.iconFarm = iconFarm;
    }

    public String getPathAlias() {
        return pathAlias;
    }

    public void setPathAlias(String pathAlias) {
        this.pathAlias = pathAlias;
    }

    public boolean isHasStats() {
        return hasStats;
    }

    public void setHasStats(boolean hasStats) {
        this.hasStats = hasStats;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    public int getFriend() {
        return friend;
    }

    public void setFriend(int friend) {
        this.friend = friend;
    }

    public int getFamily() {
        return family;
    }

    public void setFamily(int family) {
        this.family = family;
    }

    public int getRevcontact() {
        return revcontact;
    }

    public void setRevcontact(int revcontact) {
        this.revcontact = revcontact;
    }

    public int getRevfriend() {
        return revfriend;
    }

    public void setRevfriend(int revfriend) {
        this.revfriend = revfriend;
    }

    public int getRevfamily() {
        return revfamily;
    }

    public void setRevfamily(int revfamily) {
        this.revfamily = revfamily;
    }

    public Content getUsername() {
        return username;
    }

    public void setUsername(Content username) {
        this.username = username;
    }

    public Content getRealname() {
        return realname;
    }

    public void setRealname(Content realname) {
        this.realname = realname;
    }

    public Content getLocation() {
        return location;
    }

    public void setLocation(Content location) {
        this.location = location;
    }

    public Content getDescription() {
        return description;
    }

    public void setDescription(Content description) {
        this.description = description;
    }

    public Content getPhotosUrl() {
        return photosUrl;
    }

    public void setPhotosUrl(Content photosUrl) {
        this.photosUrl = photosUrl;
    }

    public Content getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(Content profileUrl) {
        this.profileUrl = profileUrl;
    }

    public Content getMobileUrl() {
        return mobileUrl;
    }

    public void setMobileUrl(Content mobileUrl) {
        this.mobileUrl = mobileUrl;
    }

    public PersonPhotos getPersonPhotos() {
        return personPhotos;
    }

    public void setPersonPhotos(PersonPhotos personPhotos) {
        this.personPhotos = personPhotos;
    }

    public String getBuddyIconUrl() {

        String buddyIconUrl;

        if (Integer.parseInt(iconServer) > 0) {
            buddyIconUrl = String.format(Locale.getDefault(), "https://farm%d.staticflickr.com/%s/buddyicons/%s.jpg", iconFarm, iconServer, nsid);
        } else {
            buddyIconUrl = "https://www.flickr.com/images/buddyicon.gif";
        }
        return buddyIconUrl;
    }
}
