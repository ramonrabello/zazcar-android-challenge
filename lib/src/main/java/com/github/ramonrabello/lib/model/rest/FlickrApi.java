package com.github.ramonrabello.lib.model.rest;

import com.github.ramonrabello.lib.model.json.PeopleInfo;
import com.github.ramonrabello.lib.model.json.PhotoInfo;
import com.github.ramonrabello.lib.model.json.PhotoLocation;
import com.github.ramonrabello.lib.model.json.PhotoSearch;
import com.github.ramonrabello.lib.model.json.PhotoStats;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Interface for Flickr API.
 */

public interface FlickrApi {

    String BASE_URL = "https://api.flickr.com/services/";

    /**
     * Calls API with flickr.photos.search method.
     *
     *
     * @param tags
     * @return
     */
    @GET("rest/?method=flickr.photos.search")
    Observable<PhotoSearch> getPhotos(@Query("tags") String tags, @Query("page") int page);

    /**
     * Calls flickr.photos.getInfo method.
     *
     * @param photoId
     *
     * @return
     */
    @GET("rest/?method=flickr.people.getInfo")
    Observable<PeopleInfo> getPeopleInfo(@Query("user_id") String photoId);

    /**
     * Calls flickr.people.getInfo endpoint.
     *
     * @param photoId
     * @param secret
     * @return
     */
    @GET("rest/?method=flickr.photos.getInfo")
    Observable<PhotoInfo> getPhotoInfo(@Query("photo_id") String photoId,
                                       @Query("secret") String secret);

    /**
     * Calls API with flickr.stats.getPhotoStats method.
     *
     * @return
     */
    @GET("rest/?method=flickr.stats.getPhotoStats")
    Observable<PhotoStats> getPhotoStats(@Query("photo_id") String photoId);
}
