package com.github.ramonrabello.lib.model.json;

import java.util.List;

/**
 * Created by ramonrabello on 25/11/16.
 */

public class Urls {

    private List<Url> urlList;

    public List<Url> getUrlList() {
        return urlList;
    }

    public void setUrlList(List<Url> urlList) {
        this.urlList = urlList;
    }
}
