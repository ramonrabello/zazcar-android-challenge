package com.github.ramonrabello.lib.model.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ramonrabello on 25/11/16.
 */

public class People {

    @SerializedName("haspeople")
    private boolean hasPeople;

    public boolean isHasPeople() {
        return hasPeople;
    }

    public void setHasPeople(boolean hasPeople) {
        this.hasPeople = hasPeople;
    }


}
