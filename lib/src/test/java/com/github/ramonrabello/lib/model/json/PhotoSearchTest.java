package com.github.ramonrabello.lib.model.json;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * Test for {@link PhotoSearch} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PhotoSearchTest {

    @Mock
    Photos mockedPhotos;

    @Before
    public void initTest(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldValidateIfPhotoSearchHasAllProperties(){
        PhotoSearch photoSearch = new PhotoSearch();
        photoSearch.setPhotos(mockedPhotos);

        // mock methods return values
        when(photoSearch.getPhotos().getPage()).thenReturn(1);
        when(photoSearch.getPhotos().getPages()).thenReturn(5);
        when(photoSearch.getPhotos().getTotal()).thenReturn("10");
        when(photoSearch.getPhotos().getPerpage()).thenReturn(5);

        // asserts that return results are ok
        assertNotNull(photoSearch.getPhotos());
        assertEquals(photoSearch.getPhotos().getPage(), 1);
        assertEquals(photoSearch.getPhotos().getPages(), 5);
        assertEquals(photoSearch.getPhotos().getTotal(), "10");
        assertEquals(photoSearch.getPhotos().getPerpage(), 5);
    }
}
