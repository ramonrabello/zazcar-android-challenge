package com.github.ramonrabello.lib.model.json;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static junit.framework.Assert.*;

/**
 * Test for {@link PhotoInfoItem} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PhotoItemTest {

    @Mock Owner owner;
    @Mock Content title;
    @Mock Content description;
    @Mock Visibility visibility;
    @Mock Dates dates;
    @Mock Editability editability;
    @Mock Editability publicEditability;
    @Mock Usage usage;
    @Mock Content comments;
    @Mock Notes notes;
    @Mock People people;
    @Mock Tags tags;
    @Mock Urls urls;
    @Mock List<Note> mockedNoteList;

    @Before
    public void initTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldValidateIfPhotoItemHasOwner(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setOwner(owner);

        when(photoInfoItem.getOwner().getIconserver()).thenReturn("123456");
        when(photoInfoItem.getOwner().getIconfarm()).thenReturn(6);
        when(photoInfoItem.getOwner().getLocation()).thenReturn("Brasília, Distrito Federal");
        when(photoInfoItem.getOwner().getNsid()).thenReturn("12345@N05");
        when(photoInfoItem.getOwner().getRealname()).thenReturn("Ramon Rabello");
        when(photoInfoItem.getOwner().getUsername()).thenReturn("ramonrabello");
        when(photoInfoItem.getOwner().getPathAlias()).thenReturn("path_alias");

        assertEquals(photoInfoItem.getOwner().getIconserver(), "123456");
        assertEquals(photoInfoItem.getOwner().getIconfarm(), 6);
        assertEquals(photoInfoItem.getOwner().getLocation(), "Brasília, Distrito Federal");
        assertEquals(photoInfoItem.getOwner().getNsid(), "12345@N05");
        assertEquals(photoInfoItem.getOwner().getRealname(), "Ramon Rabello");
        assertEquals(photoInfoItem.getOwner().getUsername(), "ramonrabello");
        assertEquals(photoInfoItem.getOwner().getPathAlias(), "path_alias");
    }

    @Test
    public void shouldValidateIfPhotoItemHasTitle(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setTitle(title);

        when(photoInfoItem.getTitle().getContent()).thenReturn("Photo title");

        assertEquals(photoInfoItem.getTitle().getContent(), "Photo title");
    }

    @Test
    public void shouldValidateIfPhotoItemHasDescription(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setDescription(description);

        when(photoInfoItem.getDescription().getContent()).thenReturn("Photo description");

        assertEquals(photoInfoItem.getDescription().getContent(), "Photo description");
    }

    @Test
    public void shouldValidateIfPhotoItemHasVisibility(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setVisibility(visibility);

        when(photoInfoItem.getVisibility().isFamily()).thenReturn(false);
        when(photoInfoItem.getVisibility().isPublic()).thenReturn(true);
        when(photoInfoItem.getVisibility().isFriend()).thenReturn(false);

        assertFalse(photoInfoItem.getVisibility().isFamily());
        assertTrue(photoInfoItem.getVisibility().isPublic());
        assertFalse(photoInfoItem.getVisibility().isFriend());

    }

    @Test
    public void shouldValidateIfPhotoItemHasDates(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setDates(dates);

        when(photoInfoItem.getDates().getLastUpdate()).thenReturn("123456432123");
        when(photoInfoItem.getDates().getPosted()).thenReturn("56655645221");
        when(photoInfoItem.getDates().getTaken()).thenReturn("54345665543");
        when(photoInfoItem.getDates().getTakenGranularity()).thenReturn("1");


        assertEquals(photoInfoItem.getDates().getLastUpdate(), "123456432123");
        assertEquals(photoInfoItem.getDates().getPosted(), "56655645221");
        assertEquals(photoInfoItem.getDates().getTaken(), "54345665543");
        assertEquals(photoInfoItem.getDates().getTakenGranularity(), "1");

    }

    @Test
    public void shouldValidateIfPhotoItemHasEditability(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setEditability(editability);

        when(photoInfoItem.getEditability().isCanAddMeta()).thenReturn(true);
        when(photoInfoItem.getEditability().isCanComment()).thenReturn(true);

        assertTrue(photoInfoItem.getEditability().isCanAddMeta());
        assertTrue(photoInfoItem.getEditability().isCanComment());
    }

    @Test
    public void shouldValidateIfPhotoItemHasPublicEditability(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setPublicEditability(publicEditability);

        when(photoInfoItem.getPublicEditability().isCanAddMeta()).thenReturn(false);
        when(photoInfoItem.getPublicEditability().isCanComment()).thenReturn(true);

        assertFalse(photoInfoItem.getPublicEditability().isCanAddMeta());
        assertTrue(photoInfoItem.getPublicEditability().isCanComment());
    }

    @Test
    public void shouldValidateIfPhotoItemHasUsage(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setUsage(usage);

        when(photoInfoItem.getUsage().isCanBlog()).thenReturn(true);
        when(photoInfoItem.getUsage().isCanDownload()).thenReturn(false);
        when(photoInfoItem.getUsage().isCanPrint()).thenReturn(false);
        when(photoInfoItem.getUsage().isCanShare()).thenReturn(true);

        assertTrue(photoInfoItem.getUsage().isCanBlog());
        assertFalse(photoInfoItem.getUsage().isCanDownload());
        assertFalse(photoInfoItem.getUsage().isCanPrint());
        assertTrue(photoInfoItem.getUsage().isCanShare());
    }

    @Test
    public void shouldValidateIfPhotoItemHasComments(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setComments(comments);

        when(photoInfoItem.getComments().getContent()).thenReturn("A photo comment");

        assertEquals(photoInfoItem.getComments().getContent(), "A photo comment");
    }

    @Test
    public void shouldValidateIfPhotoItemHasEmptyNotes(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        List<Note> noteList = new ArrayList<>();

        notes.setNoteList(noteList);
        photoInfoItem.setNotes(notes);

        assertTrue(photoInfoItem.getNotes().getNoteList().isEmpty());
    }

    @Test
    public void shouldValidateIfPhotoItemHasPeople(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        photoInfoItem.setPeople(people);

        when(photoInfoItem.getPeople().isHasPeople()).thenReturn(true);

        assertEquals(photoInfoItem.getPeople().isHasPeople(), true);
    }

    @Test
    public void shouldValidateIfPhotoItemHasTags(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        List<Tag> tagList = new ArrayList<>();
        tagList.add(new Tag());
        tagList.add(new Tag());
        tagList.add(new Tag());
        tagList.add(new Tag());
        tagList.add(new Tag());
        tags.setTagList(tagList);
        photoInfoItem.setTags(tags);

        when(photoInfoItem.getTags().getTagList()).thenReturn(tagList);

        assertEquals(photoInfoItem.getTags().getTagList().size(), 5);
    }

    @Test
    public void shouldValidateIfPhotoItemHasUrls(){

        PhotoInfoItem photoInfoItem = new PhotoInfoItem();
        List<Url> urlList = new ArrayList<>();
        urlList.add(new Url());
        urlList.add(new Url());
        urlList.add(new Url());
        urls.setUrlList(urlList);
        photoInfoItem.setUrls(urls);

        when(photoInfoItem.getUrls().getUrlList()).thenReturn(urlList);

        assertEquals(photoInfoItem.getUrls().getUrlList().size(), 3);
    }
}
