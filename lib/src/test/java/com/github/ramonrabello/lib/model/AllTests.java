package com.github.ramonrabello.lib.model;

import com.github.ramonrabello.lib.model.json.PeopleInfoTest;
import com.github.ramonrabello.lib.model.json.PhotoInfoTest;
import com.github.ramonrabello.lib.model.json.PhotoItemTest;
import com.github.ramonrabello.lib.model.json.PhotoSearchTest;
import com.github.ramonrabello.lib.model.json.PhotoStatsTest;
import com.github.ramonrabello.lib.model.json.PhotoTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import static org.junit.runners.Suite.SuiteClasses;

/**
 * Test suite for all classes.
 */
@RunWith(Suite.class)
@SuiteClasses({
        PhotoTest.class,
        PhotoInfoTest.class,
        PhotoItemTest.class,
        PhotoSearchTest.class,
        PhotoStatsTest.class,
        PeopleInfoTest.class
})
public class AllTests {

}