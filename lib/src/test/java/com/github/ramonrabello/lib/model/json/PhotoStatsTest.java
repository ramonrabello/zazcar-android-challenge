package com.github.ramonrabello.lib.model.json;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.*;


/**
 * Test class for {@link PhotoStats} class.
 */
public class PhotoStatsTest {

    private PhotoStats photoStats;

    @Before
    public void initTest(){
        photoStats = new PhotoStats();
    }

    @Test
    public void shouldValidateIfPhotoStatsHasViews(){
        photoStats.setViews("3");
        assertEquals(photoStats.getViews(), "3");
    }

    @Test
    public void shouldValidateIfPhotoStatsHasFaves(){
        photoStats.setFaves("32");
        assertEquals(photoStats.getFaves(), "32");
    }

    @Test
    public void shouldValidateIfPhotoStatsHasComments(){
        photoStats.setComments("15");
        assertEquals(photoStats.getComments(), "15");
    }
}
