package com.github.ramonrabello.lib.model.json;

import android.os.Parcel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Test class for {@link Photo} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PhotoTest {

    @Mock
    Parcel parcel;

    @Before
    public void initTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldValidateIfPhotoHasMandatoryProperties() {

        Photo photo = new Photo(parcel);

        // call methods
        photo.setId("123456");
        photo.setSecret("xxxxxxxxxx");
        photo.setAuthorName("Ramon Rabello");
        photo.setAuthorDescription("Android Developer Senior");

        // asserts that return results are ok
        assertEquals(photo.getId(), "123456");
        assertEquals(photo.getSecret(), "xxxxxxxxxx");
        assertEquals(photo.getAuthorName(), "Ramon Rabello");
        assertEquals(photo.getAuthorDescription(), "Android Developer Senior");
    }

    @Test
    public void shouldValidateIfPhotoHasValidUrl() {

        Photo photo = new Photo(parcel);

        // call methods
        photo.setFarm(6);
        photo.setServer("3456");
        photo.setId("123456");
        photo.setSecret("xxxxxxxxxx");

        // asserts that the url is in valid format
        assertEquals(photo.getUrl(), "https://farm6.staticflickr.com/3456/123456_xxxxxxxxxx.jpg");
    }

    @Test
    public void shouldValidateIfPhotoHasValidSmallSizeUrl() {

        Photo photo = new Photo(parcel);

        // call methods
        photo.setFarm(5);
        photo.setServer("3456");
        photo.setId("123456");
        photo.setSecret("xxxxxxxxxx");

        // asserts that the url is in valid format
        assertEquals(photo.getSmallSizeUrl(), "https://farm5.staticflickr.com/3456/123456_xxxxxxxxxx_s.jpg");

    }

    @Test
    public void shouldValidateIfPhotoHasValidMediumSizeUrl() {

        Photo photo = new Photo(parcel);

        // call methods
        photo.setFarm(3);
        photo.setServer("3456");
        photo.setId("123456");
        photo.setSecret("xxxxxxxxxx");

        // asserts that the url is in valid format
        assertEquals(photo.getMediumSizeUrl(), "https://farm3.staticflickr.com/3456/123456_xxxxxxxxxx_c.jpg");

    }

    @Test
    public void shouldValidateIfPhotoHasValidLargeSizeUrl() {

        Photo photo = new Photo(parcel);

        // call methods
        photo.setFarm(2);
        photo.setServer("3456");
        photo.setId("123456");
        photo.setSecret("xxxxxxxxxx");

        // asserts that the url is in valid format
        assertEquals(photo.getLargeSizeUrl(), "https://farm2.staticflickr.com/3456/123456_xxxxxxxxxx_q.jpg");
    }
}
