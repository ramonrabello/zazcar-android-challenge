package com.github.ramonrabello.lib.model.rest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

/**
 * Created by ramonrabello on 28/11/16.
 */
public class FlickrApiTest {

    private MockWebServer mockWebServer = new MockWebServer();

    // Ask the server for its URL. You'll need this to make HTTP requests.
    HttpUrl baseUrl = mockWebServer.url("/v1/chat/");


    @Test
    public void shouldTestPhotoSearchCall(){
        mockWebServer.enqueue(new MockResponse());
    }

    @Before
    public void initTest() throws IOException {
        mockWebServer.start();
    }

    @After
    public void endTest() throws IOException {
        mockWebServer.shutdown();
    }

}
