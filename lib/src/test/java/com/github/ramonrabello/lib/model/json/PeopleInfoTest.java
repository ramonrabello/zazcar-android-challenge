package com.github.ramonrabello.lib.model.json;

/**
 * Created by ramonrabello on 28/11/16.
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import static junit.framework.Assert.*;

/**
 * Test class for {@link PeopleInfo} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PeopleInfoTest {

    @Mock
    Person person;

    @Before
    public void initTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldTestIfPeopleInfoStatIsFail(){
        PeopleInfo peopleInfo = new PeopleInfo();
        peopleInfo.setStat("fail");

        assertTrue(peopleInfo.isStatFail());
    }

    @Test
    public void shouldTestIfPeopleInfoStatIsOk(){
        PeopleInfo peopleInfo = new PeopleInfo();
        peopleInfo.setStat("ok");

        assertTrue(peopleInfo.isStatOk());
    }

    @Test
    public void shouldTestIfPeopleHasPerson(){
        PeopleInfo peopleInfo = new PeopleInfo();
        peopleInfo.setPerson(person);

        when(peopleInfo.getPerson().getId()).thenReturn("123456");
        when(peopleInfo.getPerson().getIconFarm()).thenReturn(1);
        when(peopleInfo.getPerson().getIconServer()).thenReturn("123456");
        when(peopleInfo.getPerson().getNsid()).thenReturn("123456@N05");

        assertEquals(peopleInfo.getPerson().getId(), "123456");
        assertEquals(peopleInfo.getPerson().getIconFarm(), 1);
        assertEquals(peopleInfo.getPerson().getIconServer(), "123456");
        assertEquals(peopleInfo.getPerson().getNsid(), "123456@N05");

    }
}
