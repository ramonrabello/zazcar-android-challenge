package com.github.ramonrabello.lib.model.json;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import static junit.framework.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link PhotoInfo} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PhotoInfoTest {

    @Mock
    PhotoInfoItem photoInfoItem;

    @Before
    public void initTest() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void shouldValidateIfStatIsOK(){
        PhotoInfo photoInfo = new PhotoInfo();
        photoInfo.setStat("ok");

        assertTrue(photoInfo.isOk());
    }

    @Test
    public void shouldValidateIfStatIsFail(){
        PhotoInfo photoInfo = new PhotoInfo();
        photoInfo.setStat("fail");

        assertTrue(photoInfo.isFail());
    }

    @Test
    public void shouldValidateIfPhotoInfoIsComplete(){
        PhotoInfo photoInfo = new PhotoInfo();
        photoInfo.setPhotoInfoItem(photoInfoItem);

        when(photoInfo.getPhotoInfoItem().getId()).thenReturn("123");
        when(photoInfo.getPhotoInfoItem().getSecret()).thenReturn("xxx");
        when(photoInfo.getPhotoInfoItem().getServer()).thenReturn("6543");
        when(photoInfo.getPhotoInfoItem().getFarm()).thenReturn(2);
        when(photoInfo.getPhotoInfoItem().getDataUploaded()).thenReturn("234321134345");

        assertEquals(photoInfo.getPhotoInfoItem().getId(), "123");
        assertEquals(photoInfo.getPhotoInfoItem().getSecret(), "xxx");
        assertEquals(photoInfo.getPhotoInfoItem().getServer(), "6543");
        assertEquals(photoInfo.getPhotoInfoItem().getFarm(), 2);
        assertEquals(photoInfo.getPhotoInfoItem().getDataUploaded(), "234321134345");
    }
}
