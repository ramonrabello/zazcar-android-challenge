package com.github.ramonrabello.flicky;

import android.content.Context;
import android.location.Address;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * Class that holds application scope objects.
 */
public class FlickyApplication extends MultiDexApplication {

    private Address myAddress;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public Address getMyAddress() {
        return myAddress;
    }

    public void setMyAddress(Address myAddress) {
        this.myAddress = myAddress;
    }
}
