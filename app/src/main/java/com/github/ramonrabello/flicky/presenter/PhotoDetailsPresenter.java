package com.github.ramonrabello.flicky.presenter;

import com.github.ramonrabello.flicky.view.ui.PhotoDetailsActivity;
import com.github.ramonrabello.lib.model.json.PeopleInfo;
import com.github.ramonrabello.lib.model.json.Photo;
import com.github.ramonrabello.lib.model.json.PhotoStats;
import com.github.ramonrabello.lib.model.rest.ApiHandler;
import com.github.ramonrabello.lib.presenter.BasePresenter;
import com.github.ramonrabello.lib.view.BaseView;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Presenter for {@link PhotoDetailsActivity}.
 */
public class PhotoDetailsPresenter implements BasePresenter<Photo> {

    private Photo photo;
    private Throwable error;
    private BaseView<Photo> view;

    public PhotoDetailsPresenter(Photo photo) {
        this.photo = photo;
        loadPeopleInfo();
        loadPhotoStats();
    }

    @Override
    public void onLoadedData() {
        loadPeopleInfo();
        loadPhotoStats();
    }

    @Override
    public void onFinish() {

    }

    private void loadPeopleInfo(){
        ApiHandler.getApi().getPeopleInfo(photo.getOwner())
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Subscriber<PeopleInfo>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                error = e;
                updateView();
            }

            @Override
            public void onNext(PeopleInfo peopleInfo) {
                photo.setAuthorName(peopleInfo.getPerson().getRealname() != null ? peopleInfo.getPerson().getRealname().getContent() : peopleInfo.getPerson().getUsername().getContent());
                photo.setAuthorDescription(peopleInfo.getPerson().getDescription().getContent());
                photo.setAuthorBuddyIconUrl(peopleInfo.getPerson().getBuddyIconUrl());
                updateView();
            }
        });
    }

    private void loadPhotoStats(){
        ApiHandler.getApi().getPhotoStats(photo.getId())
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Subscriber<PhotoStats>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                error = e;
                updateView();
            }

            @Override
            public void onNext(PhotoStats photoStats) {
                photo.setViewsCount(Integer.parseInt(photoStats.getViews()));
                photo.setFavesCount(Integer.parseInt(photoStats.getFaves()));
                photo.setCommentsCount(Integer.parseInt(photoStats.getComments()));
                updateView();
            }
        });
    }

    @Override
    public void onUpdateView(BaseView<Photo> view) {
        this.view = view;
        updateView();
    }

//    /**
//     * Callback for getting people information.
//     */
//    private class GetPeopleInfoCallback implements Callback<PeopleInfo> {
//
//        @Override
//        public void onResponse(Call<PeopleInfo> call, Response<PeopleInfo> response) {
//
//            if (response.isSuccessful()) {
//                PeopleInfo peopleInfo = response.body();
//                photo.setAuthorName(peopleInfo.getPerson().getRealname() != null ? peopleInfo.getPerson().getRealname().getContent() : peopleInfo.getPerson().getUsername().getContent());
//                photo.setAuthorDescription(peopleInfo.getPerson().getDescription().getContent());
//                photo.setAuthorBuddyIconUrl(peopleInfo.getPerson().getBuddyIconUrl());
//                updateView();
//            }
//        }
//
//        @Override
//        public void onFailure(Call<PeopleInfo> call, Throwable t) {
//            error = t;
//            updateView();
//        }
//    }

//    /**
//     * Callback for getting people information.
//     */
//    private class GetPhotoStatsCallback implements Callback<PhotoStats> {
//
//        @Override
//        public void onResponse(Call<PhotoStats> call, Response<PhotoStats> response) {
//
//            if (response.isSuccessful()) {
//                PhotoStats photoStats = response.body();
//                photo.setViewsCount(Integer.parseInt(photoStats.getViews()));
//                photo.setFavesCount(Integer.parseInt(photoStats.getFaves()));
//                photo.setCommentsCount(Integer.parseInt(photoStats.getComments()));
//                updateView();
//            }
//        }
//
//        @Override
//        public void onFailure(Call<PhotoStats> call, Throwable t) {
//            error = t;
//            updateView();
//        }
//    }

    private void updateView() {
        if (view != null){
            if (photo != null){
                view.onDataAvailable(photo);
            } else if (error != null){
                view.onDataError(error);
            }
        }
    }
}
