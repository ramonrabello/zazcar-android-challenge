package com.github.ramonrabello.flicky.view.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.ramonrabello.androidassignment.R;
import com.github.ramonrabello.lib.view.adapter.BaseViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * View holder for {@link com.github.ramonrabello.lib.model.json.Photo}.
 */
 class PhotoViewHolder extends BaseViewHolder {

    @BindView(R.id.photo)
    ImageView photo;

    @BindView(R.id.photo_title)
    TextView photoTitle;

    @BindView(R.id.by_owner)
    TextView byOwner;

    PhotoViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }
}