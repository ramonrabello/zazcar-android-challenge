package com.github.ramonrabello.flicky.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.github.ramonrabello.androidassignment.R;
import com.github.ramonrabello.flicky.view.ui.PhotoDetailsActivity;
import com.github.ramonrabello.lib.model.json.Photo;
import com.github.ramonrabello.lib.view.adapter.BaseAdapter;

import jp.wasabeef.glide.transformations.GrayscaleTransformation;

/**
 * View holder for Flickr photos.
 */

public class PhotoAdapter extends BaseAdapter<PhotoViewHolder, Photo> {

    private Context context;

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.photo_view_holder, parent, false);
        PhotoViewHolder viewHolder = new PhotoViewHolder(itemView);
        viewHolder.setOnItemClickCallback(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        Photo photo = getItem(position);
        Glide.with(context).load(photo.getUrl())
                .bitmapTransform(new GrayscaleTransformation(context))
                .into(holder.photo);

        holder.photoTitle.setText(photo.getTitle());
        holder.byOwner.setText(context.getString(R.string.by_owner, photo.getOwner()));
    }

    @Override
    public void onItemClick(View view, int position) {
        Photo dataSelected = getItem(position);
        Intent intent = new Intent(context, PhotoDetailsActivity.class);
        intent.putExtra("photo_selected", dataSelected);

        context.startActivity(intent);
    }
}