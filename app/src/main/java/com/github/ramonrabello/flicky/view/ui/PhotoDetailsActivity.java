package com.github.ramonrabello.flicky.view.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.ramonrabello.androidassignment.R;
import com.github.ramonrabello.flicky.presenter.PhotoDetailsPresenter;
import com.github.ramonrabello.lib.model.json.Photo;
import com.github.ramonrabello.lib.presenter.BasePresenter;
import com.github.ramonrabello.lib.view.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Activity to load photo details.
 */
public class PhotoDetailsActivity extends AppCompatActivity implements BaseView<Photo> {

    private Photo photoSelected;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.backdrop)
    ImageView backdrop;

    @BindView(R.id.author_image)
    ImageView authorImage;

    @BindView(R.id.author_name)
    TextView authorName;

    @BindView(R.id.author_description)
    TextView authorDescription;

    @BindView(R.id.views_count)
    TextView viewsCount;

    @BindView(R.id.faves_count)
    TextView favesCount;

    @BindView(R.id.comments_count)
    TextView commentsCount;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private BasePresenter<Photo> presenter;
    private Unbinder unbinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_details);

        unbinder = ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.fab)
    public void onFabClicked(FloatingActionButton fab) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.photo_share_text, photoSelected.getTitle(), photoSelected.getAuthorName(), photoSelected.getPageUrl()));
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        photoSelected = getIntent().getParcelableExtra("photo_selected");
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolbar.setTitle(photoSelected.getTitle());
        if (presenter == null) {
            presenter = new PhotoDetailsPresenter(photoSelected);
        }
        presenter.onUpdateView(this);
    }

    @Override
    public void onDataAvailable(Photo photo) {
        if (!isFinishing()){
            Glide.with(this).load(photo.getMediumSizeUrl()).into(backdrop);
            Glide.with(this).load(photo.getAuthorBuddyIconUrl()).into(authorImage);
            authorName.setText(photo.getAuthorName());
            authorDescription.setText(photo.getAuthorDescription() != null ? Html.fromHtml(photo.getAuthorDescription()) : "");
            viewsCount.setText(String.valueOf(photo.getViewsCount()));
            favesCount.setText(String.valueOf(photo.getFavesCount()));
            commentsCount.setText(String.valueOf(photo.getCommentsCount()));
            progressBar.animate().alpha(0.0f).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(500).start();
        }
    }

    @Override
    public void onDataError(Throwable throwable) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error_dialog_title))
                .setMessage(getString(R.string.error_dialog_message, throwable.getMessage()))
                .setPositiveButton(getString(R.string.error_dilaog_close_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (presenter != null) {
            presenter.onFinish();
            presenter.onUpdateView(null);
        }

        if (!isChangingConfigurations()) {
            presenter = null;
        }

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}
