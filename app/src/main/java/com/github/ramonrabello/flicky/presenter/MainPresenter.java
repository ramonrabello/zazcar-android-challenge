package com.github.ramonrabello.flicky.presenter;

import android.content.Context;
import android.location.Address;

import com.github.ramonrabello.flicky.FlickyApplication;
import com.github.ramonrabello.flicky.view.ui.MainActivity;
import com.github.ramonrabello.lib.model.json.Photo;
import com.github.ramonrabello.lib.model.json.PhotoInfo;
import com.github.ramonrabello.lib.model.json.PhotoSearch;
import com.github.ramonrabello.lib.model.rest.ApiHandler;
import com.github.ramonrabello.lib.presenter.BasePresenter;
import com.github.ramonrabello.lib.view.BaseView;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Presenter for {@link MainActivity}.
 */
public class MainPresenter implements BasePresenter<List<Photo>> {

    private MainActivity view;
    private Throwable error;
    private FlickyApplication app;
    private List<Photo> newPhotos = new ArrayList<>();
    private int currentPage;

    public MainPresenter(Context context) {
        this.currentPage = 1;
        app = (FlickyApplication) context.getApplicationContext();
        loadPhotos(currentPage);
    }

    public void loadPhotos(final int page) {

        // get current address
        Address myAddress = app.getMyAddress();

        if (myAddress != null) {

            ApiHandler.getApi().getPhotos(myAddress.getLocality() != null ? myAddress.getLocality() : myAddress.getSubLocality(), page)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<PhotoSearch>() {
                        @Override
                        public void onCompleted() {
                            // do nothing
                        }

                        @Override
                        public void onError(Throwable e) {
                            error = e;
                            updateView();
                        }

                        @Override
                        public void onNext(PhotoSearch photoSearch) {

                            newPhotos = photoSearch.getPhotos().getPhoto();

                            for (final Photo photo : newPhotos) {

                                ApiHandler.getApi().getPhotoInfo(photo.getId(), photo.getSecret())
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Subscriber<PhotoInfo>() {
                                            @Override
                                            public void onCompleted() {

                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                error = e;
                                                updateView();
                                            }

                                            @Override
                                            public void onNext(PhotoInfo photoInfo) {
                                                photo.setAuthorName(photoInfo.getPhotoInfoItem().getOwner().getRealname());
                                                photo.setAuthorDescription(photoInfo.getPhotoInfoItem().getDescription().getContent());
                                            }
                                        });
                            }
                            updateView();
                        }
                    });
        }
    }

    @Override
    public void onLoadedData() {
        loadPhotos(currentPage);
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onUpdateView(BaseView<List<Photo>> view) {
        this.view = (MainActivity) view;
        updateView();
    }

    private void updateView() {
        if (view != null) {
            if (newPhotos != null) {
                view.onDataAvailable(newPhotos);
            } else if (error != null) {
                view.onDataError(error);
            }
        }
    }

    public List<Photo> getNewPhotos() {
        return newPhotos;
    }

    public Context getContext(){
        return view;
    }
}