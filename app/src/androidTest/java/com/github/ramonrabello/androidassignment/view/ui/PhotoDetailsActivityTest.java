package com.github.ramonrabello.androidassignment.view.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.github.ramonrabello.androidassignment.R;
import com.github.ramonrabello.lib.model.json.Photo;
import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.intent.Intents.*;
import static android.support.test.espresso.intent.matcher.IntentMatchers.*;
import static org.hamcrest.Matchers.*;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import okhttp3.HttpUrl;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

/**
 * UI Test for {@link PhotoDetailsActivityTest} class.
 */

@RunWith(AndroidJUnit4.class)
public class PhotoDetailsActivityTest {

    @Rule
    public IntentsTestRule<PhotoDetailsActivity> intentsTestRule = new IntentsTestRule<>(PhotoDetailsActivity.class, true, false);
    private MockWebServer mockWebServer;
    private HttpUrl url;

    @Before
    public void initTest() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @NonNull
    private Photo createMockPhoto() {
        Photo photo = new Photo();
        photo.setId("123456");
        photo.setAuthorBuddyIconUrl("");
        photo.setFarm(6);
        photo.setSecret("123456543");
        photo.setViewsCount(30);
        photo.setFavesCount(145);
        photo.setCommentsCount(22);
        photo.setAuthorName("Ramon Rabello");
        photo.setAuthorDescription("blablablabla");
        return photo;
    }

    @Test
    public void whenActivityLoaded_shouldCheckIfViewsAreWithText() throws InterruptedException {
        url = mockWebServer.url("/");

        mockWebServer.enqueue(new MockResponse()
                .setBody("{}")
                .setResponseCode(200)
                .setBodyDelay(3, TimeUnit.SECONDS));

        //RecordedRequest recordedRequest = mockWebServer.takeRequest();
        onView(withId(R.id.author_image)).check(matches(isDisplayed()));
        onView(withId(R.id.views_count)).check(matches(withText("30")));
        onView(withId(R.id.faves_count)).check(matches(withText("145")));
        onView(withId(R.id.comments_count)).check(matches(withText("22")));
        onView(withId(R.id.author_name)).check(matches(withText("Ramon Rabello")));
        onView(withId(R.id.author_description)).check(matches(withText("blablablabla")));
    }

    @Test
    public void whenActivityLoaded_andFabClicked_shouldSharePhoto(){

        Photo photo = new Photo();
        photo.setId("123456");
        photo.setTitle("Photo Title");
        photo.setAuthorName("Author Name");
        photo.setOwner("54322344");

        onView(withId(R.id.author_image)).perform(click());

        intended(allOf(
                hasAction(Intent.ACTION_SEND),
                hasType("text/plain"),
                hasExtra(Intent.EXTRA_TEXT, intentsTestRule.getActivity().getString(R.string.photo_share_text, photo.getTitle(), photo.getAuthorName(), photo.getPageUrl()))
        ));

    }

    public static Matcher<Intent> chooser(Matcher<Intent> matcher) {
        return allOf(hasAction(Intent.ACTION_CHOOSER), hasExtra(is(Intent.EXTRA_INTENT), matcher));
    }

    @After
    public void endTest() throws IOException {
        mockWebServer.shutdown();
    }
}