package com.github.ramonrabello.androidassignment.view.ui;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.github.ramonrabello.androidassignment.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.contrib.RecyclerViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;


/**
 * UI Test for {@link MainActivity} class.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void whenActivityLoaded_clickOnFistItemOnRecyclerView(){

        // android.R.id.list is the RecyclerView id inside the SuperRecyclerView
        onView(withId(android.R.id.list)).perform(actionOnItemAtPosition(0, click()));
    }

    @Test
    public void whenActivityLoaded_scrollToItemPositionOnRecyclerView(){

        // scrolls to item 4 on recyclerview and click to open PhotoDetailsActivity
        onView(withId(android.R.id.list)).perform(scrollToPosition(3), click());

        // check if all views in another Activity are visible
        onView(withId(R.id.backdrop)).check(matches(isDisplayed()));
        onView(withId(R.id.author_name)).check(matches(isDisplayed()));
        onView(withId(R.id.author_description)).check(matches(isDisplayed()));
        onView(withId(R.id.views_count)).check(matches(isDisplayed()));
        onView(withId(R.id.faves_count)).check(matches(isDisplayed()));
        onView(withId(R.id.comments_count)).check(matches(isDisplayed()));
    }
}
